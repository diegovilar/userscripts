// ==UserScript==
// @name         Forbes Ad-Remover
// @namespace    https://bitbucket.org/diegovilar/userscripts
// @downloadURL  https://bitbucket.org/diegovilar/userscripts/raw/master/forbes-ad-remover.js
// @updateURL    https://bitbucket.org/diegovilar/userscripts/raw/master/forbes-ad-remover.js
// @version      0.8.2
// @description  Removes ads from Forbes sites
// @author       Diego Vilar
// @grant        none
// @match        http://www.forbes.com/sites/*
// @run-at document-idle
// ==/UserScript==

(function() {

	var count = 10;

    var id = setInterval(function() {

		if (count--) {
			try {
				$('.rec_ad').remove();
				$('.recx_ad').remove();
				$('.recx_ad').remove();
				$('.article_body_ad').remove();
				$('.ad_initialized').remove();
			}
			catch (e) {
			}
		}
		else {
			clearInterval(id);
		}

    }, 1000);

})();
