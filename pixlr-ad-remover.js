// ==UserScript==
// @name         Pixlr Ad-Remover
// @namespace    https://bitbucket.org/diegovilar
// @downloadURL  https://bitbucket.org/diegovilar/userscripts/raw/master/pixlr-ad-remover.js
// @updateURL    https://bitbucket.org/diegovilar/userscripts/raw/master/pixlr-ad-remover.js
// @version      0.8.1
// @description  Removes ads on Pixlr
// @author       Diego Vilar
// @grant        none
// @match        http://apps.pixlr.com/editor/
// @run-at document-idle
// ==/UserScript==

(function() {
    
    try {
        var adds = document.getElementsByClassName('ad-wrap');

        if (adds.length) {
            adds[0].remove();
            document.body.style.paddingRight = 0;
        }
    }
    catch (e) {
    }
    
})();
